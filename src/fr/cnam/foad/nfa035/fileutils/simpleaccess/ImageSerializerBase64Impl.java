package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class ImageSerializerBase64Impl implements ImageSerializer {
    private final Base64.Encoder encoder;
    private final Base64.Decoder decoder;
    public ImageSerializerBase64Impl() {
        this.encoder = Base64.getEncoder();
        this.decoder = Base64.getDecoder();
    }

    @Override
    public String serialize(File image) throws IOException {
        byte[] imgContent = Files.readAllBytes( image.toPath() );
        return this.encoder.encodeToString( imgContent );
    }

    @Override
    public byte[] deserialize(String encodedImage) {
        return this.decoder.decode( encodedImage );
    }
}
