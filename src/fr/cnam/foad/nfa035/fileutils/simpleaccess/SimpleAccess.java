package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SimpleAccess {
    public static void main(String[] args)  {
        try {
            File image = new File("petite_image.png");
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            writeToFile("image_serialized_content.txt", serializer.serialize(image));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode utile pour écrire dans un fichier text
     *
     * @param filename
     * @param text
     */
    public static void writeToFile(String filename, String text) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(filename);
        byte[] strToBytes = text.getBytes();
        int lineLen = 76;

        for(int i=0; i<strToBytes.length; i+=lineLen) {
            if( i>0 ) {
                outputStream.write("\n".getBytes());
            }
            outputStream.write(strToBytes,i,Integer.min(lineLen, strToBytes.length-i));
        }
        outputStream.close();
    }
}
